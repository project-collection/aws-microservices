locals {
  global_vars = yamldecode(file("global_vars.yml"))
}

remote_state {
  backend = "s3"
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
  config = {
    bucket = "371584219818-terraform-state"

    key = "${path_relative_to_include()}/terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "terraform-lock-table"
  }
}

generate "provider" {
  path = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
    provider "aws" {
        region = "${local.global_vars["region"]}"
    }
    EOF
}

generate "label" {
  path = "label.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
    module "label" {
      source = "git::https://github.com/cloudposse/terraform-null-label.git?ref=0.22.1"
      stage = var.environment
      tags = {
        "ManagedBy" = "Terraform"
        "Namespace" = "${local.global_vars["namespace"]}"
      }
    }
    EOF
}
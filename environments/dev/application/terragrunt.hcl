locals {
  environment_specific_vars = yamldecode(file(find_in_parent_folders("environment_specific_vars.yml")))
  global_vars = yamldecode(file(find_in_parent_folders("global_vars.yml")))
}


include {
  path = find_in_parent_folders()
}

terraform {
  source = "../../../components//application"
}

inputs = {
    environment = local.environment_specific_vars["environment"]
    region      = local.global_vars["region"]
}
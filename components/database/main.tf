module "db" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~>2.0"

  identifier = "${module.label.id}-db"

  engine            = "postgres"
  engine_version    = "12.4"
  instance_class    = "db.t2.small"
  
  # Storage
  allocated_storage = 40
  storage_type = "gp2"
  storage_encrypted = false

  # block auto-update
  allow_major_version_upgrade = false
  auto_minor_version_upgrade = false

  # Security
  name     = var.db_database_name
  username = var.db_master_username
  password = var.db_master_password
  port     = "5432"
  iam_database_authentication_enabled = false

  # Networking
  publicly_accessible = false
  vpc_security_group_ids = [module.db-sg.this_security_group_id]
  create_db_subnet_group = true
  # private subnets
  subnet_ids = data.terraform_remote_state.networking.outputs.private_subnets


  # Backup and Maintenance
  apply_immediately = true
  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  # Monitoring
  monitoring_interval = "30"
  monitoring_role_name = "${module.label.id}-monitoring-role"
  create_monitoring_role = true

  # Snapshot
  final_snapshot_identifier = "${module.label.id}-snapshot"
  copy_tags_to_snapshot = true


  # Database Deletion Protection
  deletion_protection = true

  # DB parameter group
  family = "postgres12"

  # DB option group
  # postgres does not use option group but extensions
  create_db_option_group = false

  tags = module.label.tags
}


# https://docs.aws.amazon.com/AWSEC2/latest/APIReference/API_DescribeVpcs.html
data "aws_vpc" "selected" {
    filter {
        name = "tag:Name"
        values = ["dev"]
    }
    filter {
        name = "tag:Stage"
        values = [var.environment]
    }
}

# security group
module "db-sg" {
  source = "terraform-aws-modules/security-group/aws"

  vpc_id = data.aws_vpc.selected.id
  name = "${module.label.id}-db-sg"
  description = "Security group for Postgres"
  
  computed_ingress_with_source_security_group_id = [
    {
      rule                     = "postgresql-tcp"
      source_security_group_id = data.terraform_remote_state.networking.outputs.bastion_security_group_id
    }
  ]
  number_of_computed_ingress_with_source_security_group_id = 1

  tags = module.label.tags
}

# Rabbit MQ
#   module "mq_broker" {
#     source = "cloudposse/mq-broker/aws"
#     version     = "0.4.1"

#     name                         = "test-mq-broker"
#     apply_immediately            = true
#     auto_minor_version_upgrade   = false
#     deployment_mode              = "SINGLE_INSTANCE"
#     engine_type                  = "RabbitMQ"
#     # https://docs.aws.amazon.com/amazon-mq/latest/developer-guide/broker-engine.html
#     engine_version               = "3.8.6"
#     host_instance_type           = "mq.t3.micro"
#     publicly_accessible          = false
#     general_log_enabled          = true
#     audit_log_enabled            = true
#     encryption_enabled           = false
#     use_aws_owned_key            = true
#     vpc_id                       = data.aws_vpc.selected.id
#     subnet_ids                   = data.terraform_remote_state.networking.outputs.private_subnets
    
#     use_existing_security_groups = true
#     existing_security_groups      = [module.rabbitmq-sg.this_security_group_id]
# }

#   # security group
# module "rabbitmq-sg" {
#   source = "terraform-aws-modules/security-group/aws"

#   vpc_id = data.aws_vpc.selected.id
#   name = "${module.label.id}-rabbitmq-sg"
#   description = "Security group for RabbitMQ"
  
#   computed_ingress_with_source_security_group_id = [
#     {
#       rule                     = "rabbitmq"
#       source_security_group_id = data.terraform_remote_state.networking.outputs.bastion_security_group_id
#     }
#   ]
#   number_of_computed_ingress_with_source_security_group_id = 1

#   tags = module.label.tags
# }

# rabbitmq test instance   Username: test  Password:123456123456

# ECR Repository

# https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-amazonmq-broker.html
# {
#   "Description": "Create a basic ActiveMQ broker",
#   "Resources": {
#     "BasicBroker": {
#       "Type": "AWS::AmazonMQ::Broker",
#       "Properties": {
#         "AutoMinorVersionUpgrade": "false",
#         "BrokerName": "MyBasicActiveBroker",
#         "DeploymentMode": "SINGLE_INSTANCE",
#         "EngineType": "ActiveMQ",
#         "EngineVersion": "5.15.0",
#         "HostInstanceType": "mq.t2.micro",
#         "PubliclyAccessible": "true",
#         "Users": [
#           {
#             "ConsoleAccess": "true",
#             "Groups": [
#               "MyGroup"
#             ],
#             "Password" : { "Ref" : "AmazonMqPassword" },
#             "Username" : { "Ref" : "AmazonMqUsername" }
#           }
#         ]
#       }
#     }
#   }
# }
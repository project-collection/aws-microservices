variable "environment" {
    description = "deployment environment dev|staging|prod"
}

variable "region" {
    description = "deployment region"
}


variable "db_database_name" {
    description = "database name for the postgres database"
}

variable "db_master_username" {
    description = "master username for the postgres database"
}

variable "db_master_password" {
    description = "master password for the postgres database"
}
===========
application
===========


.. contents::


Build Application Manually
==========================

instal docker
-------------

.. code:: bash

  sudo amazon-linux-extras install docker
  sudo service docker start
  sudo usermod -a -G docker ec2-user
  # Make docker auto-start
  sudo chkconfig docker on
  # Reboot to verify it all loads fine on its own.
  sudo reboot


install docker-compose
----------------------

https://gist.github.com/npearce/6f3c7826c7499587f00957fee62f8ee9

.. code:: bash

  sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose
  docker-compose version


push image to ecr
-----------------


.. code:: bash

  aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 695782613344.dkr.ecr.us-east-1.amazonaws.com
  docker tag test:latest 695782613344.dkr.ecr.us-east-1.amazonaws.com/test:latest
  docker push 695782613344.dkr.ecr.us-east-1.amazonaws.com/test:latest




Install Source Code
===================

git clone
---------

.. code:: bash

  git clone https://tian2@bitbucket.org/bgshep/etl-service.git

branch: etl11

etl-service
-----------

* set environment variables

.. code:: bash

  export PRIVATE_PYPI_USER=
  export PRIVATE_PYPI_PASSWORD=


* build image

.. code:: bash

  make build
  make deploy-etl11

deploy-etl11 is branch-dependent. create new branch have to refit the following block in Makefile

.. code:: bash

  .PHONY: deploy-etl11aws
  deploy-etl11aws: source_docker_compose_yaml_file = docker-compose-dev.yaml
  deploy-etl11aws: branch = origin/etl11aws
  deploy-etl11aws: deploy


* create config file /etc/etl-service.env (that file need to exist on the docker host)

.. code:: bash

  ETL_SERVICE_DTN_LOGIN=xxxx
  ETL_SERVICE_DTN_PASSWORD=xxxx
  ETL_SERVICE_DATA_CONSUMERS=['postgres_consumer']
  ETL_SERVICE_RABBITMQ_HOST=xxxx
  ETL_SERVICE_RABBITMQ_USER=xxxx
  ETL_SERVICE_RABBITMQ_PASSWORD=xxxx
  ETL_SERVICE_RABBITMQ_PORT=xxxx
  ETL_SERVICE_RABBITMQ_SSL=xxxx
  ETL_SERVICE_te_postgres_connection_string=xxxx
  ETL_SERVICE_te_postgres_connection_pool_size=2
  ETL_SERVICE_te_postgres_connection_pool_max_over_flow=0
  ETL_SERVICE_SYMBOLS={'JTNT.Z': 0, '@VX#': 0, '@ES#C': 0, 'JVNT.Z': 0, 'JINT.Z': 0, 'JTYT.Z': 0, '@NQ#C': 0, 'JV5T.Z': 0, 'JIQT.Z': 0, 'JTQT.Z': 0, '@RTY#C': 0, 'JTRT.Z': 0, 'JVRT.Z': 0, 'JIRT.Z': 0, '@YM#C': 0, 'QQQ': 0, 'XLF': 0, 'AMZN': 0, 'SPY': 0, 'AAPL': 0, 'GOOGL': 0, 'MSFT': 0, 'FB': 0, 'XLE': 0, 'RSP': 0, 'IWM': 0, 'FINL.Z': 0, 'NFLX': 0, 'NVDA': 0, 'XLU': 0, 'XLK': 0, 'XLV': 0, 'XLY': 0, 'CRM': 0, 'MA': 0, 'V': 0, 'PYPL': 0, 'ADBE': 0, 'INTC': 0, 'TSLA': 0}
  ETL_SERVICE_SEND_TICKERS=["@ES#C", "@RTY#C", "@NQ#C","@YM#C"]
  ETL_SERVICE_LOGSTASH_HOST=10.138.155.184
  ETL_SERVICE_ENVIRONMENT=dev
  ETL_SERVICE_NON_TRADING_DATES=[]
  ETL_SERVICE_SENTRY_LOG_LEVEL=WARNING
  ETL_SERVICE_TIMESTAMP_HEARTBEAT_MONITOR_URL=https://heartbeat.uptimerobot.com/m784720791-800b7dc8ed7b05397fc905fe7565a3c04883ddc4
  ETL_SERVICE_TICK_DATA_HEARTBEAT_MONITOR_URL=https://heartbeat.uptimerobot.com/m784720800-5d95c9041257c063b61f4749ed2f0adc71c2e9d8


SQL Postgres Migration
======================

.. code:: sql

  CREATE TABLE public.bars_1m
  (
    id serial PRIMARY KEY,
    created TIMESTAMPTZ default now(),
    dt timestamp without time zone NOT NULL,
    instrument varchar(16) not null,
    open double precision,
    high double precision,
    low double precision,
    close double precision,
    volume integer,
    upticks integer,
    downticks integer,
    upvolume integer,
    downvolume integer
  );
  CREATE INDEX IF NOT EXISTS bars_1m_dt_idx ON public.bars_1m (dt);
  CREATE INDEX IF NOT EXISTS bars_1m_dt_instrument_idx ON public.bars_1m (dt, instrument);
  CREATE UNIQUE INDEX IF NOT EXISTS dt_instrument_bars_1m_idx ON public.bars_1m (dt, instrument);
  CREATE TABLE public.bars_2m
  (
    id serial PRIMARY KEY,
    created TIMESTAMPTZ default now(),
    dt timestamp without time zone NOT NULL,
    instrument varchar(16) not null,
    open double precision,
    high double precision,
    low double precision,
    close double precision,
    volume integer
  );
  CREATE INDEX IF NOT EXISTS bars_2m_dt_idx ON public.bars_2m (dt);
  CREATE INDEX IF NOT EXISTS bars_2m_dt_instrument_idx ON public.bars_2m (dt, instrument);
  CREATE UNIQUE INDEX IF NOT EXISTS dt_instrument_bars_2m_idx ON public.bars_2m (dt, instrument);
  CREATE TABLE public.bars_5m
  (
    id serial PRIMARY KEY,
    created TIMESTAMPTZ default now(),
    dt timestamp without time zone NOT NULL,
    instrument varchar(16) not null,
    open double precision,
    high double precision,
    low double precision,
    close double precision,
    volume integer
  );
  CREATE INDEX IF NOT EXISTS bars_5m_dt_idx ON public.bars_5m (dt);
  CREATE INDEX IF NOT EXISTS bars_5m_dt_instrument_idx ON public.bars_5m (dt, instrument);
  CREATE UNIQUE INDEX IF NOT EXISTS dt_instrument_bars_5m_idx ON public.bars_5m (dt, instrument);
  CREATE TABLE public.bars_15m
  (
    id serial PRIMARY KEY,
    created TIMESTAMPTZ default now(),
    dt timestamp without time zone NOT NULL,
    instrument varchar(16) not null,
    open double precision,
    high double precision,
    low double precision,
    close double precision,
    volume integer
  );
  CREATE INDEX IF NOT EXISTS bars_15m_dt_idx ON public.bars_15m (dt);
  CREATE INDEX IF NOT EXISTS bars_15m_dt_instrument_idx ON public.bars_15m (dt, instrument);
  CREATE UNIQUE INDEX IF NOT EXISTS dt_instrument_bars_15m_idx ON public.bars_15m (dt, instrument);
  CREATE TABLE public.bars_30m
  (
    id serial PRIMARY KEY,
    created TIMESTAMPTZ default now(),
    dt timestamp without time zone NOT NULL,
    instrument varchar(16) not null,
    open double precision,
    high double precision,
    low double precision,
    close double precision,
    volume integer
  );
  CREATE INDEX IF NOT EXISTS bars_30m_dt_idx ON public.bars_30m (dt);
  CREATE INDEX IF NOT EXISTS bars_30m_dt_instrument_idx ON public.bars_30m (dt, instrument);
  CREATE UNIQUE INDEX IF NOT EXISTS dt_instrument_bars_30m_idx ON public.bars_30m (dt, instrument);
  ALTER table public.bars_2m
  ADD COLUMN IF NOT EXISTS upticks integer,
  ADD COLUMN IF NOT EXISTS downticks integer,
  ADD COLUMN IF NOT EXISTS upvolume integer,
  ADD COLUMN IF NOT EXISTS downvolume integer;
  ALTER table public.bars_5m
  ADD COLUMN IF NOT EXISTS upticks integer,
  ADD COLUMN IF NOT EXISTS downticks integer,
  ADD COLUMN IF NOT EXISTS upvolume integer,
  ADD COLUMN IF NOT EXISTS downvolume integer;
  ALTER table public.bars_15m
  ADD COLUMN IF NOT EXISTS upticks integer,
  ADD COLUMN IF NOT EXISTS downticks integer,
  ADD COLUMN IF NOT EXISTS upvolume integer,
  ADD COLUMN IF NOT EXISTS downvolume integer;
  ALTER table public.bars_30m
  ADD COLUMN IF NOT EXISTS upticks integer,
  ADD COLUMN IF NOT EXISTS downticks integer,
  ADD COLUMN IF NOT EXISTS upvolume integer,
  ADD COLUMN IF NOT EXISTS downvolume integer;


data "terraform_remote_state" "networking" {
  backend = "s3"
  config = {
    bucket = "371584219818-terraform-state"
    key    = "${var.environment}/networking/terraform.tfstate"
    region = var.region
  }
}
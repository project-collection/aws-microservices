data "template_file" "etl_service" {
    template = file(var.task_definition_path)

    vars = {
        log_group_name = var.log_group_name
        region_name = var.region
        awslogs_stream_prefix = var.service_name
        account_id = var.account_id
        environment = var.environment
        name        = var.service_name
        image       = var.task_image
    }
}



# start ecs service with access from bastion
resource "aws_ecs_service" "this" {
  name                                  = var.service_name
  task_definition                       = aws_ecs_task_definition.this.arn
  cluster                               = var.cluster_arn
  desired_count                         = 1
  deployment_maximum_percent            = 200
  deployment_minimum_healthy_percent    = 100

  force_new_deployment = true

  deployment_controller {
    type                = "ECS"
  }

  network_configuration {
    subnets             = var.ecs_service_subnets
    security_groups     = var.ecs_service_security_groups
    assign_public_ip    = true
  }

  launch_type                           = "FARGATE"
  scheduling_strategy                   = "REPLICA"

  lifecycle {
    ignore_changes   = [desired_count]
  }

  tags = var.tags

  depends_on         = [aws_ecs_task_definition.this]
}



resource "aws_ecs_task_definition" "this" {
  family                        = var.service_name
  execution_role_arn            = var.execution_role_arn
  task_role_arn                 = module.ecs_task_iam_role.this_iam_role_arn
  container_definitions         = data.template_file.etl_service.rendered
  cpu                           = 1024
  memory                        = 2048

  requires_compatibilities      = toset(["FARGATE"])
  network_mode                  = "awsvpc"
  tags                          = var.tags
}




module "ecs_task_iam_role" {
  source = "terraform-aws-modules/iam/aws//modules/iam-assumable-role"
  version = "~> 3.0"

  role_name         = "ecs-task-etl-service"
  role_requires_mfa = false
  create_role = true

  trusted_role_services = ["ecs-tasks.amazonaws.com"]

  custom_role_policy_arns = [
    aws_iam_policy.this.arn,
  ]
  number_of_custom_role_policy_arns = 1
  tags = var.tags

}

resource "aws_iam_policy" "this" {
  name        = "ECSTaskETLServicePolicy"
  path        = "/"
  description = "ECS Task IAM Policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "kms:Decrypt",
        "secretsmanager:GetSecretValue",
        "ssm:GetParameter"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF
}
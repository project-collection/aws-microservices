variable "service_name" {
    description = "name of the ecs service"
}

variable "cluster_arn" {
    description = "arn of the ecs cluster"
}

variable "task_definition_path" {
    description = "path to task definition path"
}

variable "task_image" {
    description = "Docker image for task"
}

variable "execution_role_arn" {
    description = "ECS task execution role arn"
}

variable "account_id" {
    description = "account_id"
}

variable "environment" {
    description = "deployment environment"
}

variable "region" {
    description = "deployment region"
}

variable "ecs_service_security_groups" {
    type = list
    description = "ECS Service security groups"
}

variable "ecs_service_subnets" {
    type = list
    description = "ECS Service subnets"
}

variable "log_group_name" {
    description = "log group name"
}

variable "tags" {
    description = "tags"
}
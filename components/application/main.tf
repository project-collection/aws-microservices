data "aws_caller_identity" "current" {}



data "aws_iam_policy_document" "ecs_tasks_execution_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ecs_tasks_execution_role" {
  name               = "${var.environment}-ecs-task-execution-role"
  assume_role_policy = data.aws_iam_policy_document.ecs_tasks_execution_role.json
  tags = module.label.tags
}

resource "aws_iam_role_policy_attachment" "ecs_tasks_execution_role" {
  role       = aws_iam_role.ecs_tasks_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_cloudwatch_log_group" "etl" {
  name = var.environment
  tags = module.label.tags
}



resource "aws_ecs_cluster" "etl" {
  name = "etl"
  tags = module.label.tags
}



resource "aws_ecr_repository" "etl_service" {
  name                 = "7cheetahs/${var.environment}/etl-service"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
  tags = module.label.tags
}

# ETL process
module "etl-service" {
  source = "./module-ecs-service"
  cluster_arn = aws_ecs_cluster.etl.arn
  service_name = "etl-service"
  task_definition_path = "${path.module}/task-definitions/etl-service.json"
  task_image = aws_ecr_repository.etl_service.repository_url
  execution_role_arn = aws_iam_role.ecs_tasks_execution_role.arn
  ecs_service_security_groups =  [data.terraform_remote_state.networking.outputs.bastion_security_group_id]
  ecs_service_subnets = data.terraform_remote_state.networking.outputs.public_subnets
  account_id = data.aws_caller_identity.current.account_id
  region = var.region
  environment = var.environment
  log_group_name = aws_cloudwatch_log_group.etl.name
  tags = module.label.tags
}
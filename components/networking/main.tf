
module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "~> v2.0"

  name = "dev"
  cidr = "10.0.0.0/16"

  azs             = ["${var.region}a", "${var.region}b", "${var.region}c"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  # nat gateway
  enable_nat_gateway = true
  one_nat_gateway_per_az = true
  single_nat_gateway = false
  enable_vpn_gateway = true

  # dns
  enable_dns_hostnames = true
  enable_dns_support = true

  tags = module.label.tags
}

# bastion server
module "bastion-sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 3.0"

  name        = "bastion-sg"
  description = "Security group for bastion EC2 instance"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["ssh-tcp"]
  egress_rules        = ["all-all"]

  tags = module.label.tags
}

data "aws_ami_ids" "amazon_linux" {
  owners = ["amazon"]
  filter {
    name = "name"
    values = ["amzn2-ami-hvm-2.0.20201218.1-x86_64-gp2"]
  }

  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
}

module "bastion" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "bastion"
  instance_count         = 1
  
  ami                    = data.aws_ami_ids.amazon_linux.ids[0]
  instance_type          = "t3.medium"
  key_name               = "bastion"
  monitoring             = true
  vpc_security_group_ids = [module.bastion-sg.this_security_group_id]


  subnet_id              = module.vpc.public_subnets[0]
  associate_public_ip_address = true
  disable_api_termination = true
  ebs_optimized = true

  tags = module.label.tags
}
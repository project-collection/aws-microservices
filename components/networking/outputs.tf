output "private_subnets" {
    value = module.vpc.private_subnets
    description = "List of IDs of private subnet"
}

output "public_subnets" {
    value = module.vpc.public_subnets
    description = "List of IDs of private subnet"
}

output "bastion_security_group_id" {
    value = module.bastion-sg.this_security_group_id
    description = "Security Group ID of bastion server"
}
# cloudwatch technical user
resource "aws_iam_user" "developer" {
  name = "developer"
  path = "/"
  force_destroy = false
  tags = module.label.tags
}

module "iam_group_with_policies" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-group-with-policies"
  version = "~> 3.0"

  name = "developer"

  group_users = [
    aws_iam_user.developer.name
  ]

  attach_iam_self_management_policy = false

  custom_group_policy_arns = [
    module.iam-policy-developer.arn,
  ]
}

data "aws_iam_policy_document" "developer" {
    statement {
        sid = "FullAccessCloudwatchLogs"
        actions = [
            "logs:*"     
        ]
        resources = [
           "*"
        ]
    }
}

# policy cloudwatch logs readonly
module "iam-policy-developer" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-policy"
  version = "~> 3.0"

  name        = "Developer"
  path        = "/"
  description = "Developer Access to Cloudwatch Logs"

  policy = data.aws_iam_policy_document.developer.json
}
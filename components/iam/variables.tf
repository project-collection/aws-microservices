variable "environment" {
    description = "deployment environment dev|staging|prod"
}

variable "region" {
    description = "deployment region"
}

=================
aws-microservices
=================


.. contents::


Project Structure
=================

deployment folder
-----------------

where the "real" deployment locates (terragrunt creates in each deployment folder a .terragrunt-cache folder)

.. code:: bash

    environments/
        global_vars.yml
        terragrunt.hcl
        dev/
            environment_specific_vars.yml
            terragrunt.hcl
            application/
                terragrunt.hcl
            networking/
                terragrunt.hcl
        staging/
        prod/


configuration file:

:global_vars.yml: across-environment variables 
:environment_specific_vars.yml: environment specific variables



component folder
----------------

where the development of terraform code is

.. code:: bash

    components
        application/
        networking/
        database/
        iam/



Architecture
====================

Account Architecture
--------------------

.. image:: docs/account-architecture.png


Software Architecture
---------------------

.. image:: docs/software-architecture.png

Tagging Concept
---------------

for the resource categorization and billing purpose, all cost-related related resources should be tagged with 

:ManagedBy: Terraform
:Namespace: 7cheetahs 
:Stage: dev | staging | prod

tagging is technically solved by https://github.com/cloudposse/terraform-null-label.git

Parameter Store
---------------

naming convetion in parameter store

.. code:: bash

    /${namespace}/${environment_name}/${project_name}/${application_name}


manually register the secrets into parameter store


* /7cheetahs/dev/etl/ETL_SERVICE_DTN_LOGIN
* /7cheetahs/dev/etl/ETL_SERVICE_DTN_PASSWORD
* /7cheetahs/dev/etl/ETL_SERVICE_RABBITMQ_HOST
* /7cheetahs/dev/etl/ETL_SERVICE_RABBITMQ_PASSWORD
* /7cheetahs/dev/etl/ETL_SERVICE_RABBITMQ_PORT
* /7cheetahs/dev/etl/ETL_SERVICE_RABBITMQ_SSL
* /7cheetahs/dev/etl/ETL_SERVICE_RABBITMQ_USER
* /7cheetahs/dev/etl/ETL_SERVICE_te_postgres_connection_string


Miscellenous
============

install tools
-------------

* psql

.. code:: bash

    sudo amazon-linux-extras install postgresql10


* pika

.. code:: bash

    sudo yum install -y python-pip
    sudo pip install pika


* test postgres connection

.. code:: bash

    psql -h dev-db.csvjx0vpmyfu.us-east-1.rds.amazonaws.com -p 5432 -U test -d test


:password: testtest

* test rabbitmq connection

.. code:: bash

    # UI
    curl -v https://b-ab9727d9-447d-4041-b864-43bb6f4f3448.mq.us-east-1.amazonaws.com


Reference
=========

* [ssh login into fargate container](https://medium.com/ci-t/9-steps-to-ssh-into-an-aws-fargate-managed-container-46c1d5f834e2)
* [ssh enabled ubuntu 18.04 dockerfile example](https://gitlab.com/ricardomendes/docker-ssh-aws-fargate/-/blob/master/Dockerfile)
* [get the latest aws ami ids with terraform](https://letslearndevops.com/2018/08/23/terraform-get-latest-centos-ami/)
* [How To Install and Manage RabbitMQ](https://www.digitalocean.com/community/tutorials/how-to-install-and-manage-rabbitmq)
* [RabbitMQ](https://www.rabbitmq.com/devtools.html)
* [Tutorial RabbitMQ](https://www.rabbitmq.com/tutorials/tutorial-one-python.html)
* [How to install Mongodb on AWS EC2 Instance?](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-amazon/)
* [How to install RabbitMQ on AWS EC2 Instance](https://dev.to/ashutosh049/install-rabbitmq-on-amamzon-ec2-amazon-linux-2-3dpd)
* [Docker iqfeed](https://github.com/jaikumarm/docker-iqfeed)
